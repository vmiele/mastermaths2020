\documentclass[10pt]{beamer}
\usetheme{default}
\usepackage[utf8]{inputenc}
\usepackage[frenchb]{babel}
\usecolortheme{fly}
\usepackage{multicol}
\usepackage{url}

\renewcommand\shapedefault{sc}
\renewcommand\bfdefault{m}

\newcommand{\jup}{\ \includegraphics[height=0.3cm]{figures/jupyter}}
\newcommand{\R}{\texttt{R} }
\newcommand{\demo}{\begin{center}\textcolor{red}{DEMO}\end{center} }
\newcommand{\pkg}[1]{\texttt{#1} }
\newcommand{\youtube}[1]{\begin{center}\textcolor{blue}{watch VIDEOS x #1 :}\end{center} }
\newcommand{\video}[2]{\href{#1}{\beamergotobutton{\textcolor{blue}{#2}}}}

\setbeamertemplate{itemize items}{>>}
\setbeamertemplate{itemize subitem}[square]
\setbeamertemplate{itemize subsubitem}[ball]

\usepackage{tikz}

\setbeamertemplate{button}{\tikz
	\node[
	color=black,
	inner xsep=10pt,
	draw=structure!80,
	fill=structure!50,
	rounded corners=4pt]  {\normalsize\insertbuttontext};}
%\setbeamercolor{button}{bg=black,fg=yellow}

\usepackage{hyperref}
\makeatletter
\newcommand\dirref[2]{\hyper@linkurl{#2}{#1}}
\makeatother

%%%%%%%%%%%%%%%% TITLE %%%%%%%%%%%%%%%%
\title[R calcul]{
	\bf{Best Practices for Scientific Computing}
}

\author{Vincent Miele - vincent.miele@univ-lyon1.fr}
\institute{CNRS - Biométrie \& Biologie Evolutive (LBBE)\\\vspace{0.25cm}}
\date{}
\begin{document}
\begin{frame}{}
\titlepage
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Programming, a skill for the modern mathematician 2.0}
\begin{itemize}[<+->]
	\item big data era, IA era
	\item reproductibility era
	\item visualization era
\end{itemize}
\begin{block}{}
	\only<2>{\href{papers/ecolett-ponisio2017_exple_an_1.pdf}{\beamergotobutton{exemple}}}
	\only<2>{\href{papers/nature-ince2012_program_reproductibilty_an.pdf}{\beamergotobutton{open computer program}}}
	\only<2>{\href{papers/Elevating_The_Status_of_Code_in_Ecology_an.pdf}{\beamergotobutton{elevating status of code}}}
	\only<3>{\href{https://www.datavisualizationsociety.com/challenge}{\beamergotobutton{vizualization society}}}
	\only<3>{\href{https://d3js.org/}{\beamergotobutton{d3js}}}
\end{block} 
\youtube{2}
\video{https://www.youtube.com/watch?v=PxyG2W5jgzs}{Jean Zay, vers la convergence de deux mondes}
\video{https://www.youtube.com/watch?v=gVUvnSJ-t3M}{How to Create 3D Objects With Python And Blender}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Programming, what does it mean?}
\begin{itemize}[<+->]
	\item talking to the computer
	\item telling it rigorously what it has to do
	\item understanding the computer's answer
\end{itemize}
\begin{block}{in other words}
	\only<1>{choosing a language}
	\only<2>{writing in this language in a well-organized way} 
	\only<3>{testing, debuging, optimizing}
\end{block} 
\end{frame}

\begin{frame}{Code optimization: now?}
	Code optimization: all the efforts to improve performance, i.e increasing execution speed and/or decrease/limit the memory footprint ({\it i.e.} the quantity of required memory).\\\vspace{0.25cm}
	Herb Sutter: 	\begin{center}{\it \og{}never optimize prematurely\fg{}.}
		\end{center}	
	And: 	\begin{center}{\it\og{}Correctness, simplicity, and clarity come first\fg{}.}
		\href{https://www.oreilly.com/library/view/c-coding-standards/0321113586/ch07.html}{\beamergotobutton{KISS}}
	\end{center}	\pause
	Vincent Miele:
	\begin{center}{\it 
			\og{}We are not surprised enough by the good performance of a wrong code!\fg{}}
	\end{center}	
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Règle 1 :choosing a language}
\begin{center}
	{\texttt{C/C++, Fortran, Python,  Matlab, R, Julia} ?} \\
\end{center}
\end{frame}

\begin{frame}{\textcolor{red}{Recall: langage typology des langages}}
\only<1>{Language {\it level}: how much the programmer has to know how a computer work.}

\only<2>{{\it Compiled} language: when the code is transformed into binary code by another program called the {\it compiler}.}

\only<3>{{\it Interpreted} language: when the code is translated and executed "on the fly" by another program called the "interpreter".}

\only<4>{{\it strongly typed} language: all variables must be  declared (type, sign, size) with no conversion allowed.}

\only<5>{{\it weakly typed} language: the opposite}

\only<6>{{\it Procedural} language:  based upon the concept of the procedure call,  also known as routines, subroutines, or functions, with global data.}

\only<7>{{\it Object-oriented} language: allows to  group data and functionalities in small independent entities (classes).}

\only<7>{Which language is what?}

\youtube{1}
\video{https://www.youtube.com/watch?v=4lXp_89c3RU}{Différence entre un interpréteur et un compilateur ?}
\end{frame}

\begin{frame}{Rule 1 : choosing but not choosing a language}
\begin{center}
	{\texttt{C/C++, Fortran, Python,  Matlab, R, Julia} ?} \\
\end{center}
% TODO SONDAGE point important
A pragmatic approach, multiple choices allowed*: your turn! \pause
\begin{multicols}{2}	
	\begin{itemize}[<+->]
		\item learning time
		\item development speed
		\item extra libraries/packages availability
		\item scientific ecosystem
		\item performance
		\item security
		\item business model
		\item interoperability
		\item adaptability to parallel computing
	\end{itemize}
\end{multicols}
\vspace{0.25cm}	\pause
{\scriptsize (*) On the web :	\og{}I don't think there is a general answer.\fg{}
	\og{}It probably depends on your culture.\fg{} 
	\og{}The more tools you have the more effectively you can do the job.\fg{}
	\og{}Every tool has its place.\fg{} }
\end{frame}

\begin{frame}{Rule 1 : choosing but not choosing a language}
\href{papers/ploscompbiol-carey2018_an_1.pdf}{\beamergotobutton{pick a language}}
% DEMO Google trends (incl. Grenoble vs Clermont): R, Python, C++, shell
\href{https://trends.google.fr}{\beamergotobutton{trends}}

% DEMO rapidité
\href{code/lang/lang2.R}{\beamergotobutton{Version R}}
\href{code/lang/lang2.py}{\beamergotobutton{Version Python}}
\href{code/lang/lang2.cpp}{\beamergotobutton{Version C++}}
% DEMO syntaxe
\href{code/lang/lang4.py}{\beamergotobutton{Version Python}}
\href{code/lang/lang4.cpp}{\beamergotobutton{Version C++}}
% ADAPTE?
\href{https://besjournals.onlinelibrary.wiley.com/doi/abs/10.1111/2041-210X.12713}{\beamergotobutton{adapted to the communauty}}
%TODO DEMO RCPP simple
%\href{code/TODO}{\beamergotobutton{TODO R \& C++}}
\href{http://localhost:8888/notebooks/Rcalcul-chapitre1.ipynb}{\beamergotobutton{R \& C++ DEMO C++}}
\youtube{2}
\video{https://www.youtube.com/watch?v=eRP_J2yLjSU}{R versus Python}
\video{https://www.youtube.com/watch?v=ncLemYw8LUs}{JIT with Numba in Python}
\video{https://www.youtube.com/watch?v=ceQ9CXuY7FY}{Why Julia is the future of programming}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Rule 2 : code versioning}
  {\it Version control}\index{gestionnaire de version} is mandatory!! \\\vspace{0.25cm}
	\begin{itemize}
	\item record all the changes perfomed on the files
	\item allow spearate {\it branches of development}
\item manage merges between files jointly modified by a team
	\end{itemize}
\end{frame}
\begin{frame}{Rule 2 : code versioning}
Do you (still) undo?\\
\href{https://openclassrooms.com/fr/courses/2342361-gerez-votre-code-avec-git-et-github}{\beamergotobutton{Version control - intro}}
\href{https://www.youtube.com/watch?v=rP3T0Ee6pLU}{\beamergotobutton{Git}}
\href{https://gitlab.com}{\beamergotobutton{gitlab demo}}
\href{http://git-school.github.io/visualizing-git/}{\beamergotobutton{git animé}}
\href{https://agripongit.vincenttunru.com}{\beamergotobutton{git animé 2 -> A vous!}} % A BOSSER CAR PAS SIMPLE

{\tiny 
\begin{itemize}
	\item  git commit -m "On avance"
	\item git log
	\item git branch alter
	\item git checkout alter
	\item git commit
	\item git checkout master
	\item git merge alter
\end{itemize}
}
\youtube{3}
\video{https://www.youtube.com/watch?v=rP3T0Ee6pLU}{Comprendre Git}
\video{https://www.youtube.com/watch?v=hwP7WQkmECE}{Git Explained in 100 Seconds}
\video{https://www.youtube.com/watch?v=w3jLJU7DT5E}{What is github?}
\end{frame}

\begin{frame}{Rule 2 : code versioning}
They don't undo:\\
\href{https://github.com/stouffer}{\beamergotobutton{exple1}}
\href{https://github.com/facebook}{\beamergotobutton{exple2}}
\href{https://github.com/aursiber}{\beamergotobutton{exple3}}
\href{papers/ploscompbiol-perez2016_git_github_an1.pdf}{\beamergotobutton{advantages of git}}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Rule 3 : naming convention and explicit naming}
\href{papers/examen_ecrit_IRE01_2018.pdf}{\beamergotobutton{exam INRA}}\\

\href{code/badcop.R}{\beamergotobutton{bad cop}}

\href{code/goodcop.R}{\beamergotobutton{good cop}}\\\pause
Coherence AND autodocumentation!
\end{frame}

\begin{frame}{Rule 3 : naming convention and explicit naming}
Write the naming conventions in a document/README : \href{https://developer.mozilla.org/fr/docs/Mozilla/Developer_guide/Coding_Style}{\beamergotobutton{@Mozilla}}
\href{https://bioconductor.org/developers/how-to/coding-style/}{\beamergotobutton{@Bioconductor}}
\pause\\
En \texttt{R}:\\
{\scriptsize
	\begin{tabular}{|c|l|}
		\hline
		Name of the convention & Principle \\
		\texttt{+ exampl}e & \\
		\hline
		\hline
		alllowercase & lower case\\
		\texttt{adjustcolor}&coherent but less readable\\
		\hline
		period.separated & lower case, words separated by a dot\\
		\texttt{plot.new}& only \texttt{R}\\
		\hline
		underscore\_separated &  lower case, words separated by underscore\\
		\texttt{seq\_along}&  recommanded by GNU\\
		\hline
		lowerCamelCase & first letter in lower case\\
		\texttt{colMeans}& words separated by upper case letters \\
		\hline
		UpperCamelCase & same but first letter in upper case\\
		\texttt{Matrix} class& usually for classes \\
		& o combine with lowerCamelCase\\
		\hline
		Bioconductor & \url{https://www.bioconductor.org/}\\
		& \url{developers/how-to/coding-style/}\\
		\hline
	\end{tabular}
}

\youtube{2}
\video{https://www.youtube.com/watch?v=Uw95Uc3xgWU}{Python Naming Conventions}
\video{https://www.youtube.com/watch?v=Pv5dfsHBBKE}{The current state of naming conventions in R}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Rule 4 : making tests}
\begin{center}
\only<1>{\it 
		\og{}The unit of measure for the progress of a code development is not the number of lines, but the number of lines that has been tested and that work!.\fg{}
}
\only<2>{\it 
	\og{} It’s not that we don’t test our code, it’s that we don’t store our tests so they can be re-run automatically.
	\fg{} (Wickam 2011)}
\end{center}
% test pour le process de dev
\only<3>{designed on a  paper, with fixed data and parameters 
\href{papers/plosbiol-wilson2014_an1.pdf}{\beamergotobutton{testing}}
\href{papers/cell-yurkovich2017_padawan_an1.pdf}{\beamergotobutton{tests written before}}
\href{papers/ploscompbiol-taschuk2017_an_1.pdf}{\beamergotobutton{checking program robustness}}
}
\youtube{1}
\video{https://www.youtube.com/watch?v=1Lfv5tUGsn8}{Unit Tests in Python}
\end{frame}

\begin{frame}[containsverbatim]{Rule 4 : making tests} 
Packages \texttt{RUnit} et \texttt{testthat} :\\\vspace{0.25cm}

automatic tests, in particular unit tests (correspond to modules or classes). 
\begin{verbatim}
	test_that("Produces the correct log likelihood.", {
		expect_equal(object=compute.loglikelihood("data_test1.txt"), 
		expected=-772.0688, tolerance = 1e-8)
	})
\end{verbatim}
\texttt{R CMD check} for packages

\href{code/TPhclustdemo/hclustdemo/hclustdemo.Rproj}{\beamergotobutton{démo}}
\end{frame}

\begin{frame}{Rule 4 : making tests}
NB: Be carefull with machine precision\\
\href{code/arithmetik.R}{\beamergotobutton{machine precision}}
\href{https://stackoverflow.com/questions/2769510/numeric-comparison-difficulty-in-r}{\beamergotobutton{numeric comparison}}

\youtube{2}
\video{https://www.youtube.com/watch?v=PZRI1IfStY0}{Floating point}
\video{https://www.youtube.com/watch?v=wlf6egTIWiY}{Floating Point Number Type in Python}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Rule 5 = don't reinvent the wheel}
\href{papers/plosbiol-wilson2014_an3.pdf}{\beamergotobutton{reuse code}}
\href{papers/ploscompbiol-taschuk2017_an_2.pdf}{\beamergotobutton{reuse software}}
\href{papers/ploscompbiol-carey2018_an_2.pdf}{\beamergotobutton{don't reinvent the wheel}}
\href{https://cran.r-project.org/web/packages/available_packages_by_name.html}{\beamergotobutton{CRAN}}
\href{http://www.netlib.org/utk/people/JackDongarra/la-sw.html}{\beamergotobutton{linear algebra librairies}}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Règle 6 = small steps and modularity} 
The code progresses by small units (pieces of code)
(1 unit =  1 test)\\\vspace{0.25cm}
\begin{center}
	\includegraphics[height=4cm]{figures/escargot}
\end{center}

\href{papers/plosbiol-wilson2014_an2.pdf}{\beamergotobutton{incremental changes}}
\href{papers/arxiv-agile_an1.pdf}{\beamergotobutton{agile}}
\href{papers/ploscompbiol-carey2018_an_2.pdf}{\beamergotobutton{baby steps}}\\\vspace{0.15cm}


\href{papers/ploscompbiol-perez2016_an_2.pdf}{\beamergotobutton{GitLab issues (0 email!)}}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Rule 7 = bugs tracking}
\begin{center}
	{\it Finding a bug is a process of confirming the many things that you believe are true - until you find one which is not true.} (Norm Matloff)
\end{center}
\begin{enumerate}
	\item Realize there is a bug
	\item Make the bug repeatable
	\item Trouver la section de code qui conduit au bug	
	\item Modify the code
\end{enumerate}
\begin{center}{\huge $\Downarrow$}\end{center}
\begin{enumerate}
	\item Tests
	\item Tests
	\item Debogueur
	\item Tests
\end{enumerate}
\end{frame}
\begin{frame}{Rule 7 = bugs tracking}
  What does a debugger?
  \begin{itemize}
\item Determine the sequence that leads to the bug
\item Watch the variables content
\item Put breakpoints
\item Run the code step by step
\end{itemize}
\end{frame}

\begin{frame}{Rule 7 = bugs tracking}
\href{papers/ploscompbiol-carey2018_an_4.pdf}{\beamergotobutton{Ask questions}}
\href{https://hackernoon.com/the-decline-of-stack-overflow-7cb69faa575d}{\beamergotobutton{Thanks, stackoverflow!}}
\href{code/TPhclustdemo/hclustdemo/hclustdemo.Rproj}{\beamergotobutton{demo}}

\youtube{1}
\video{https://www.youtube.com/watch?v=ChuU3NlYRLQ}{Python debug}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Rule 8 = release your code}
Yes, it is worth!\\
\href{papers/nature-barnes2010_publish_your_code_an1.pdf}{\beamergotobutton{Publish your code}}
\end{frame}

\begin{frame}{Rule 8 = release your code (with a licence)}
%Qui peut utiliser un logiciel ?
\begin{block}{\href{https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069414&idArticle=LEGIARTI000006279172}{Art. L. 335-2 du CPI}}
Toute personne utilisant, copiant, modifiant ou diffusant le logiciel
sans autorisation explicite du \underline{détenteurs des droits patrimoniaux} est
coupable de contrefaçon et passible de trois ans d’emprisonnement
et de 300000 euros d’amende.
%{\tiny Source : \url{https://www.projet-plume.org/ressource/guide-logiciels-libres-administrations}}
\end{block}


\href{papers/cell-yurkovich2017_padawan_an2.pdf}{\beamergotobutton{understand licensing}}

\href{papers/licences-LL-administration.pdf}{\beamergotobutton{Les licences}}
\href{papers/preslicencesdroitsauteur_envol_vtgd.pdf}{\beamergotobutton{et aussi}}\\
\href{https://choosealicense.com/}{\beamergotobutton{Choose a licence}}\\
%https://www.gnu.org/licenses/licenses.fr.html
\dirref{file:///home/vmiele/Bureau/dynSBM_CEM/}{\beamergotobutton{Example 1}}
\dirref{file:///home/vmiele/Bureau/Silix/}{\beamergotobutton{Example 2}}

\youtube{1}
\video{https://www.youtube.com/watch?v=GlAnKGBnhFY}{How to choose a license for open scientific data and code?}
\end{frame}


\begin{frame}{Rule 8 = release your code (with an install procedure)}
Follow the standards: \texttt{install.packages, conda install, pip install, make install, python setup.py install, docker run...}\\
\href{papers/ploscompbiol-taschuk2017_an_3.pdf}{\beamergotobutton{Rely on build tools}}
\href{http://calcul.math.cnrs.fr/spip.php?article257}{\beamergotobutton{Packages R par A.Siberchicot}}
\href{docker.pdf}{\beamergotobutton{Docker}}
\\\vspace{1cm}
\pause
+bring test cases ({\it e.g.} notebook \texttt{Jupyter})\\
\href{papers/cell-yurkovich2017_padawan_an3.pdf}{\beamergotobutton{share your code}}

\youtube{1}
\video{https://www.youtube.com/watch?v=wCGsLqHOT2I}{Python Applications Packaging with Setuptools}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Rule 9 : measuring before optimising}	
Donald Knuth :
\begin{center}{\it 
		\og{} Programmers waste enormous amounts of time thinking about, or worrying about, the speed of noncritical parts of their programs
		\fg{}}
\end{center}
\href{http://wiki.c2.com/?PrematureOptimization}{\beamergotobutton{Premature optimization}}
\pause\\
Methodoloy:
\begin{enumerate} 
	\item  tests 
	\item  tools to measure performance in time and memory
\end{enumerate}
\end{frame}

\begin{frame}{\textcolor{red}{Digression: Jupyter}}
	\youtube{1}
\video{https://www.youtube.com/watch?v=2eCHD6f_phE}{Jupyter}
\end{frame}



\begin{frame}{Rule 9 : measuring before optimising}	
timer:\\
shell \texttt{time}, command \texttt{system.time}, package \texttt{microbenchmark}\\
\href{http://localhost:8888/notebooks/Rcalcul-chapitre1.ipynb}{\beamergotobutton{demo}}
\end{frame}

\begin{frame}[containsverbatim]{Rule 9 : measuring before optimising}	
Code profiling:  finding the few lines of codes responsible for the majority of the computing time (the {\it hotspots})
		\begin{columns}
		\begin{column}{6cm}
			\begin{verbatim}
			faire.courses <- fun(){
			boulangerie()
			poissonnerie()
			}
			boulangerie <- fun(){
			faire.la.queue()
			commander()
			payer()
			}
			poissonnerie <- fun(){
			faire.la.queue()
			commander()
			payer()
			}
			\end{verbatim}
		\end{column}
		\begin{column}{4cm}
			\begin{verbatim}
			$by.self
			self.time
			faire.la.queue   6
			commander        2
			payer            2
			
			$by.total            
			total.time
			faire.courses   10
			faire.la.queue   6
			boulangerie      5
			poissonnerie     5
			commander        2
			payer            2
			\end{verbatim}
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}{Rule 9 : measuring before optimising}	
	Code profiling: finding the few lines of codes responsible for the majority of the computing time (the {\it hotspots}), with a{\it profiler} (sampling):\\
	\href{https://sourceware.org/binutils/docs/gprof/}{\beamergotobutton{C++}}
	\href{https://docs.python.org/3/library/profile.html}{\beamergotobutton{python}}
	\href{https://stat.ethz.ch/R-manual/R-devel/library/utils/html/Rprof.html}{\beamergotobutton{R}}
	\href{http://localhost:8888/notebooks/Rcalcul-chapitre1.ipynb}{\beamergotobutton{demo1 DEMO}}
	\href{code/TPhclustdemo/hclustdemo/hclustdemo.Rproj}{\beamergotobutton{demo2}}
\end{frame}


\begin{frame}{Rule 9 : measuring before optimising}	
Monitor memory use:\\
l'outil  \texttt{top} (Linux et Mac) ou  \texttt{Process Explorer} (Windows)
	package \pkg{pryr}  \\
	\href{http://localhost:8888/notebooks/Rcalcul-chapitre1.ipynb}{\beamergotobutton{demo}}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Règle 10 : refactoring to optimize}

\end{frame}	
	
\begin{frame}{\textcolor{red}{Rappel: complexity}}
The {\it algorithmic complexity} is a theoretical order of magnitude of the computing time and the memory as a fonction of the data size. \\
\vspace{0.5cm}{\it Notation Landau}:  the positive function $g$ is $O(f)$ if there exists 
constantes $c>0$ and $x_0>>0$ such that $g(x) < c*f(x)$ for all $x > x_0$.
We note $g = O(f)$ \\
\vspace{0.5cm}
Example :\\
dichotomic search in a sorted list $O(log(n))$\\
search in a list $O(n)$\\
\href{papers/nphard.pdf}{\beamergotobutton{NP-complete}}

\youtube{1}
\video{https://www.youtube.com/watch?v=bKRngCvb1Do}{Complexité algorithmique}
\end{frame}

\begin{frame}{Règle 10 : refactoring to optimize}
Code "refactoring"\\
\vspace{0.5cm}
{\it e.g.}
\begin{itemize}
	\item Change your algorithm : better complexity (for instance \texttt{Matrix} vs \texttt{sparseMatrix}) \pause
	\item Use {\it open source} code from the community \pause
	\item Think about memory problems...
\end{itemize}
\end{frame}


\begin{frame}{Règle 10 : refactoring to optimize}	
	\href{code/cache.R}{\beamergotobutton{mystery...}}
\end{frame}

\begin{frame}{\textcolor{red}{Recall: computer architecture}}
	\only<1>{The CPU (Central Processing Unit) / processor is in charge of processing data.}
	\only<2>{Clock is a signal used to sync things inside the computer.}
	\only<3>{A clock of 100 MHz means that in one second there is 100 million clock cycles.}
	\only<4>{The CPU knows how many clock cycles each instruction will take.}
\end{frame}

\begin{frame}{\textcolor{red}{Recall: computer architecture}}
	\only<1>{In an ideal world, data would come from memory at the same speed than the CPU.}
	\only<2>{This memory exists: {\it register}. So what?}
	\only<3>{As it is very expensive, it is small!}
	\only<4>{The more a type of memory is efficient, the less there are in computers}
	\only<5>{There exists a memory hierarchy, less and less efficient but larger and larger.}
	\only<6>{\includegraphics[height=3.9cm]{figures/caches}\includegraphics[height=3.9cm]{figures/nbcycles}}
	\only<7>{When the CPU requires a data, it loads it in the cache memory }
	\only<8>{A {\it line of cache} will then contain all the elements contiguous to this data. }
	\only<9>{We talk about {\it spatial locality}.\\
		\href{code/cache.R}{\beamergotobutton{spatial locality matters!}}
	}
\end{frame}




\begin{frame}[containsverbatim]{Règle 10 : refactoring to optimize}
\begin{verbatim}
my.dist <- as.dist(matrix(rnorm(1000*1000),1000,1000)); 
Rprof(); 
hclust(my.dist); 
Rprof(NULL); 
summaryRprof()

$by.self
self.time self.pct total.time total.pct
".Fortran"      0.04      100       0.04       100

$by.total
total.time total.pct self.time self.pct
".Fortran"       0.04       100      0.04      100
"hclust"         0.04       100      0.00        0


\end{verbatim}
\end{frame}

\begin{frame}{Règle 10 : refactoring to optimize}
Code "refactoring"
\vspace{0.5cm}\\
{\it e.g.}
\begin{itemize}[<+->]	
	\item hotspots rewritten in compiled language\\
	\href{code/TPhclustdemo/hclustdemo/hclustdemo.Rproj}{\beamergotobutton{demo1}}
	\href{http://localhost:8888/notebooks/Rcalcul-chapitre1.ipynb}{\beamergotobutton{demo2}}
	%\item penser parallèle?
\end{itemize}
\begin{block}{ }
\only<1>{\href{https://docs.python.org/3.7/extending/extending.html}{\beamergotobutton{Python \& C++}}}
\only<1>{\href{https://cran.r-project.org/doc/manuals/r-devel/R-exts.html\#System-and-foreign-language-interfaces}{\beamergotobutton{R \& C++}}}
\only<1>{\href{https://software.intel.com/en-us/mkl-linux-developer-guide-calling-lapack-blas-and-cblas-routines-from-c/c-language-environments}{\beamergotobutton{C++ \& Fortran}}}
\end{block} 
\end{frame}


\begin{frame}{Règle 10 : refactoring to optimize}
Code "refactoring"
\vspace{0.5cm}\\
{\it e.g.}
\begin{itemize}[<+->]	
	\item hotspots rewritten in compiled language\\
\end{itemize}
Usual scheme:
	\begin{enumerate}
		\item data load and pre-treatment in a high level interpreted language
		\item computing part with  a low level compiled language
		\item post-treatement, visualization and data write in a high level interpreted language
	\end{enumerate}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\begin{frame}{Règle 10 : refactoring to optimize}
Code "refactoring"
\vspace{0.5cm}\\
       {\it e.g.}
\begin{itemize}[<+->]	
	\item think parallel?
\end{itemize}
\end{frame}


%\begin{frame}{\textcolor{red}{Rappel: architecture des ordinateurs (suite)}}
%Fréquence d'une unité de calcul:
%\begin{center}
%	\includegraphics[height=4cm]{figures/evolution_freq}
%\end{center}	
%\href{https://fr.wikipedia.org/wiki/Loi_de_Moore}{\beamergotobutton{\scriptsize 2ème Loi de Moore = nb transistors / puce X 2 / 2 ans (toujours valide)}}
%\href{http://www.gotw.ca/publications/concurrency-ddj.htm}{\beamergotobutton{the free lunch is over (Sutter, 2005)}}
%\end{frame}
%
%\begin{frame}{Règle 11 : penser parallèle}
%\begin{center}
%\includegraphics[height=2.4cm]{figures/puzzle}\\\pause
%\begin{tabular}{cc}
%	{\Huge $\Downarrow$} & {\Huge $\Downarrow$}\\
%	\includegraphics[height=3cm]{figures/shiva}&
%	\includegraphics[height=3cm]{figures/shiva2}
%	\includegraphics[height=3cm]{figures/shiva2}
%	\includegraphics[height=3cm]{figures/shiva2}
%	\includegraphics[height=3cm]{figures/shiva2}\\
%	machine multi-coeurs & cluster
%\end{tabular}
%\end{center}
%\end{frame}


\end{document}
