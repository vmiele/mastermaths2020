#include <Rcpp.h>
using namespace Rcpp;

// [[Rcpp::export]]
NumericVector cpp_find_closest_elements(NumericVector matrix, int n){
  Rcpp::NumericVector distasvector(matrix); // COLUMNWIZE
  double min = 1e20;
  int kmin = 0;
  for (int k=0; k<n*n; k++){
    if (distasvector[k]>0 && distasvector[k]<min){
      min = distasvector[k];
      kmin = k;
    }
  }
  int jmin = kmin/n; // COLUMNWIZE
  int imin = kmin-jmin*n; // COLUMNWIZE
  Rcpp::NumericVector ijmin(2);
  if(imin>jmin){
    int tmp=imin;imin=jmin;jmin=tmp;
  }
  ijmin[0] = imin+1;
  ijmin[1] = jmin+1;
  return(ijmin);
}
