library(Rcpp)

# 1er algorithme
cppFunction('NumericVector rowSumsIJ(NumericMatrix x){
  int n = x.nrow();
  int m = x.ncol();
  NumericVector rsums(n);
  for (int i=0; i<n; i++)
    for (int j=0; j<m; j++)
      rsums[i] += x(i,j);
  return rsums;
}')

# 2ème algorithme
cppFunction('NumericVector rowSumsJI(NumericMatrix x){
  int n = x.nrow();
  int m = x.ncol();
  NumericVector rsums(n);
  for (int j=0; j<m; j++)
    for (int i=0; i<n; i++)
      rsums[i] += x(i,j);
  return rsums;
}')

n <- 3000
x <- matrix(rnorm(n*n),n,n)

library(microbenchmark)
microbenchmark(rowSumsIJ(x),    
               rowSumsJI(x), 
               20 )


system.time( rowSums(x) )[3]
system.time( colSums(x) )[3]
