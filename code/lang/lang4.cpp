#include<fstream>
#include<iostream>
#include<map>
#include<string>
#include<sstream>
using namespace std;
int main()
{
  ifstream f;
  f.open("lang4.dat");
  map<string, int> dico;
  
  string line;
  while (!f.eof()){
    getline(f, line); 
    istringstream linestream(line);
    string a; 
    int b;
    linestream>>a;    
    linestream>>b;
    dico[a] = b;
  }

  for (map<string, int>::iterator iter=dico.begin();
       iter!=dico.end(); iter++){
    cout<<iter->first<<" pour la cle "<<iter->second<<endl;
  }
}
