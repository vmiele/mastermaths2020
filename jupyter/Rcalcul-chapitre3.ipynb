{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# FIL ROUGE\n",
    "Supposons que l'on souhaite **estimer la qualité de prédiction d'un modèle linéaire**, ici un modèle linéaire pour la régression de la largeur d'une pétale sur sa longueur sur le jeu de données iris de R.\n",
    "\n",
    "Un technique envisageable est le **leave-one-out** qui consiste à estimer l'erreur de généralisation (erreur faite pour des nouveaux individus qui viennent de la même distribution que les individus utilisés pour apprendre le modèle). \n",
    "\n",
    "Le principe est simple: \n",
    "* on estime le modèle avec tous les individus sauf un, \n",
    "* on fait une prédiction pour cette individu, \n",
    "* et on regarde l'erreur quadratique  (PRESS (Prediction Sum of Squares) statistic) entre la prédiction et la valeur connue; \n",
    "* on répète l'opération pour chacun des individus et on somme les erreurs obtenues."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "leave.one.out <- function(i){\n",
    "    model <- lm(Petal.Width ~ Petal.Length, data=iris[-i,])\n",
    "    pred.petal.width <- predict(model,\n",
    "        data.frame(Petal.Length=iris[i,\"Petal.Length\"]))\n",
    "    return((pred.petal.width-iris[i,\"Petal.Width\"])^2)\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Version séquentielle"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "library(microbenchmark)\n",
    "print(microbenchmark(\n",
    "    lapply(1:150, FUN=function(i) leave.one.out(i)),\n",
    "    times=10\n",
    "))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Version parallèle"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "library(parallel)\n",
    "cl <- makeCluster(2, type=\"FORK\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(microbenchmark(\n",
    "    parLapply(cl, 1:150, fun=function(i) leave.one.out(i)),\n",
    "    times=10\n",
    "))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "stopCluster(cl)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### ou bien"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(microbenchmark(\n",
    "    mclapply(1:150, FUN=function(i) leave.one.out(i), mc.cores=2),\n",
    "    times=10\n",
    "))      "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Génération de nombres pseudo-aléatoires"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "library(parallel)\n",
    "n <- 100\n",
    "nb.simu <- 100\n",
    "x <- rnorm(n,0,1)\n",
    "y <- rnorm(n,1+2*x,2)\n",
    "data <- data.frame(x,y)\n",
    "cl <- makeCluster(4, type=\"FORK\")\n",
    "\n",
    "clusterSetRNGStream(cl)\n",
    "\n",
    "bootstrap.coef <- parLapply(cl, 1:nb.simu, fun=function(i) {\n",
    "    bootstrap.resample <- data[sample(n,n,replace=T),]\n",
    "    lm(y~x,bootstrap.resample)$coef\n",
    "})\n",
    "\n",
    "stopCluster(cl)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# FIL ROUGE (suite)\n",
    "Supposons maintenant que l'on souhaite réaliser l'approche leave-one-out sur **plusieurs jeux de données** en parallèle. \n",
    "\n",
    "Il est naturel d'envisager un parallélisme **gros-grain** qui consiste à exécuter sur chaque jeu de données la version séquentielle de la fonction de calcul de l'erreur PRESS."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "leave.one.out <- function(i, dataset){\n",
    "    model <- lm(Petal.Width ~ Petal.Length, data=dataset[-i,])\n",
    "    pred.petal.width <- predict(model,\n",
    "        data.frame(Petal.Length=dataset[i,\"Petal.Length\"]))\n",
    "    return((pred.petal.width-dataset[i,\"Petal.Width\"])^2)\n",
    "}\n",
    "compute.PRESS <- function(dataset){\n",
    "    Reduce(\"+\", lapply(1:nrow(dataset), \n",
    "                        FUN=function(i) leave.one.out(i, dataset)))\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Pour illustrer l'approche, on simule des jeux de données en nombre et de tailles différentes (indiquées dans le vecteur sizes)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "simuData <- function(sizes){\n",
    "    model <- lm(Petal.Width ~ Petal.Length, data=iris)\n",
    "    lapply(sizes, FUN=function(n){ # n nombre de points\n",
    "        a <- min(iris[,\"Petal.Length\"])\n",
    "        b <- max(iris[,\"Petal.Length\"])\n",
    "        iris2 <- data.frame(Petal.Width=rep(NA,n),\n",
    "                            Petal.Length=runif(n,a,b))\n",
    "        iris2[,\"Petal.Width\"] <-\n",
    "               predict(model,data.frame(Petal.Length=iris2[,\"Petal.Length\"])+rnorm(n,0,0.1))\n",
    "        iris2\n",
    "    })\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Ordonnancement statique versus dynamique"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "multiple.iris <- simuData(rep(200,6))\n",
    "print(microbenchmark(\n",
    "    mclapply(multiple.iris, \n",
    "             FUN=function(dataset) compute.PRESS(dataset), \n",
    "             mc.cores=2),\n",
    "    mclapply(multiple.iris, \n",
    "             FUN=function(dataset) compute.PRESS(dataset), \n",
    "             mc.cores=2, mc.preschedule=F),\n",
    "    times=5\n",
    "))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "multiple.iris <- \n",
    "   simuData(c(500,100,300,150,100,400))\n",
    "print(microbenchmark(\n",
    "    mclapply(multiple.iris, \n",
    "             FUN=function(dataset) compute.PRESS(dataset), \n",
    "             mc.cores=2),\n",
    "    mclapply(multiple.iris, \n",
    "             FUN=function(dataset) compute.PRESS(dataset), \n",
    "             mc.cores=2, mc.preschedule=F),\n",
    "    times=5\n",
    "))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "R",
   "language": "R",
   "name": "ir"
  },
  "language_info": {
   "codemirror_mode": "r",
   "file_extension": ".r",
   "mimetype": "text/x-r-source",
   "name": "R",
   "pygments_lexer": "r",
   "version": "3.6.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
